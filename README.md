# README #

Repositorio del [Meetup WordPress Cartagena](https://www.meetup.com/WordPress-Cartagena/), en España.
Inscríbete para participar en las actividades del meetup.

### ¿Qué es un meetup? ###

Los meetups son reuniones periódicas presenciales de grupos de usuarios de WordPress.
Algunos grupos de meetup son apoyados (y administrados) desde la Fundación WordPress, son los grupos oficiales.

### Contribución ###

* Participando en los meetups presenciales
* Revisando el código

### Contacto ###

* <info@wpcartagena.org>
* <https://www.meetup.com/WordPress-Cartagena/>